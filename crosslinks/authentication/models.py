from django.contrib.auth.signals import user_logged_in
from django.contrib.auth.signals import user_logged_out
from django.contrib.auth.models import User
import zmq
from django.db import models
from django.db.models import F
class Count(models.Model):
    count = models.IntegerField(null = False)
def change_counter(flag ):
    count = Count.objects.all()[0]                
    count.count = count.count + flag
    count.save()
    socket.send("{count} user online ".format(count=count.count))
   
def login_callback(sender, user, request, **kwargs):
    print(user)
    print("Logged in")
    change_counter(1)

def logout_callback(sender, user, request, **kwargs):
    print(user)
    print("Logged out")
    change_counter(-1)

if len(Count.objects.all()) ==0:
    Count(count=0).save()

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.connect("tcp://127.0.0.1:8988")
user_logged_in.connect(login_callback)
user_logged_out.connect(logout_callback)
